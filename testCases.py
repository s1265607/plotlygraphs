from dash import Dash, dcc, html
from dash.dependencies import Input, Output
import pandas as pd
import plotly.express as px
import dash_bootstrap_components as dbc


case1 = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/TestCase1ExitBoth.csv')
linecase1 = px.line(case1, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 1', markers=True)
linecase1.update_yaxes(title='time')
mean1right = case1['time- exitRight'].mean()
mean1right = round(mean1right, 1)
mean1left = case1['time- exitLeft'].mean()
mean1left = round(mean1left, 1)
std1right = case1['time- exitRight'].std()
std1right = round(std1right, 1)
std1left = case1['time- exitLeft'].std()
std1left = round(std1left, 1)
var1right = case1['time- exitRight'].var()
var1right = round(var1right, 1)
var1left = case1['time- exitLeft'].var()
var1left = round(var1left, 1)

stats1 = pd.DataFrame({
    'Average': [mean1right, mean1left],
    'Standard Deviation': [std1right, std1left],
    'Variance': [var1right, var1left]
},
    index=['Exit Right', 'Exit Left'])


case2 = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc2newStaticExitBoth.csv')
linecase2 = px.line(case2, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 2 Static', markers=True)
linecase2.update_yaxes(title='time')
mean2right = case2['time- exitRight'].mean()
mean2right = round(mean2right, 1)
mean2left = case2['time- exitLeft'].mean()
mean2left = round(mean2left, 1)
std2right = case2['time- exitRight'].std()
std2right = round(std2right, 1)
std2left = case2['time- exitLeft'].std()
std2left = round(std2left, 1)
var2right = case2['time- exitRight'].var()
var2right = round(var2right, 1)
var2left = case2['time- exitLeft'].var()
var2left = round(var2left, 1)

case2Dynamic = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc2newAfterDynamicChangesFullData.csv')
linecase2Dynamic = px.line(case2Dynamic, x='tokens', y=[
                           'time- exitRight', 'time- exitLeft'], title='Test Case 2 Dynamic', markers=True)
linecase2Dynamic.update_yaxes(title='time')
mean2Dynamicright = case2Dynamic['time- exitRight'].mean()
mean2Dynamicright = round(mean2Dynamicright, 1)
mean2Dynamicleft = case2Dynamic['time- exitLeft'].mean()
mean2Dynamicleft = round(mean2Dynamicleft, 1)
std2Dynamicright = case2Dynamic['time- exitRight'].std()
std2Dynamicright = round(std2Dynamicright, 1)
std2Dynamicleft = case2Dynamic['time- exitLeft'].std()
std2Dynamicleft = round(std2Dynamicleft, 1)
var2Dynamicright = case2Dynamic['time- exitRight'].var()
var2Dynamicright = round(var2Dynamicright, 1)
var2Dynamicleft = case2Dynamic['time- exitLeft'].var()
var2Dynamicleft = round(var2Dynamicleft, 1)



case3 = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc3StaticExitBoth.csv')
linecase3 = px.line(case3, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 3 Static', markers=True)
linecase3.update_yaxes(title='time')
mean3right = case3['time- exitRight'].mean()
mean3right = round(mean3right, 1)
mean3left = case3['time- exitLeft'].mean()
mean3left = round(mean3left, 1)
std3right = case3['time- exitRight'].std()
std3right = round(std3right, 1)
std3left = case3['time- exitLeft'].std()
std3left = round(std3left, 1)
var3right = case3['time- exitRight'].var()
var3right = round(var3right, 1)
var3left = case3['time- exitLeft'].var()
var3left = round(var3right, 1)





case3Dynamic = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc3StaticDynamicExitBoth.csv')
linecase3Dynamic = px.line(case3Dynamic, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 3 Dynamic', markers=True)
linecase3Dynamic.update_yaxes(title='time')
mean3Dynamicright = case3Dynamic['time- exitRight'].mean()
mean3Dynamicright = round(mean3Dynamicright, 1)
mean3Dynamicleft = case3Dynamic['time- exitLeft'].mean()
mean3Dynamicleft = round(mean3Dynamicleft, 1)
std3Dynamicright = case3Dynamic['time- exitRight'].std()
std3Dynamicright = round(std3Dynamicright, 1)
std3Dynamicleft = case3Dynamic['time- exitLeft'].std()
std3Dynamicleft = round(std3Dynamicleft, 1)
var3Dynamicright = case3Dynamic['time- exitRight'].var()
var3Dynamicright = round(var3Dynamicright, 1)
var3Dynamicleft = case3Dynamic['time- exitLeft'].var()
var3Dynamicleft = round(var3Dynamicright, 1)


case4 = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/TestCase4ExitBoth.csv')
linecase4 = px.line(case4, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 4', markers=True)
linecase4.update_yaxes(title='time')
mean4right = case4['time- exitRight'].mean()
mean4right = round(mean4right, 1)
mean4left = case4['time- exitLeft'].mean()
mean4left = round(mean4left, 1)
std4right = case4['time- exitRight'].std()
std4right = round(std4right, 1)
std4left = case4['time- exitLeft'].std()
std4left = round(std4left, 1)
var4right = case4['time- exitRight'].var()
var4right = round(std4right, 1)
var4left = case4['time- exitLeft'].var()
var4left = round(var4left, 1)


case5 = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc5ExitBothStatic.csv')
linecase5 = px.line(case5, x='tokens', y=[
                    'time- exitRight', 'time- exitLeft'], title='Test Case 5 Static', markers=True)
linecase5.update_yaxes(title='time')
mean5right = case5['time- exitRight'].mean()
mean5right = round(mean5right, 1)
mean5left = case5['time- exitLeft'].mean()
mean5left = round(mean5left, 1)
std5right = case5['time- exitRight'].std()
std5right = round(std5right, 1)
std5left = case5['time- exitLeft'].std()
std5left = round(std5left, 1)
var5right = case5['time- exitRight'].var()
var5right = round(var5right, 1)
var5left = case5['time- exitLeft'].var()
var5left = round(var5left, 1)

case5Dynamic = pd.read_csv(
    'https://cssegit.monmouth.edu/s1265607/plotlygraphs/-/raw/main/tc5ExitBothDynamic.csv')
linecase5Dynamic = px.line(case5Dynamic, x='tokens', y=[
                           'time- exitRight', 'time- exitLeft'], title='Test Case 5 Dynamic', markers=True)
linecase5Dynamic.update_yaxes(title='time')
mean5Dynamicright = case5Dynamic['time- exitRight'].mean()
mean5Dynamicright = round(mean5Dynamicright, 1)
mean5Dynamicleft = case5Dynamic['time- exitLeft'].mean()
mean5Dynamicleft = round(mean5Dynamicleft, 1)
std5Dynamicright = case5Dynamic['time- exitRight'].std()
std5Dynamicright = round(std5Dynamicright, 1)
std5Dynamicleft = case5Dynamic['time- exitLeft'].std()
std5Dynamicleft = round(std5Dynamicleft, 1)
var5Dynamicright = case5Dynamic['time- exitRight'].var()
var5Dynamicright = round(var5Dynamicright, 1)
var5Dynamicleft = case5Dynamic['time- exitLeft'].var()
var5Dynamicleft = round(var5Dynamicleft, 1)


app = Dash(__name__, external_stylesheets=[dbc.themes.LITERA],
           meta_tags=[{'name': 'viewpoint',
                       'content': 'width=device-width, initial-scale=1.0'}]
           )

app.layout = html.Div([
    dbc.Row([
        dbc.Col(html.H1("Test Case Graphs", className='text-center'))
    ]),

    dcc.Tabs(id='tabs-example-1', value='tab-1', children=[
        dcc.Tab(label='Test Case 1', value='tab-1'),
        dcc.Tab(label='Test Case 2 Static', value='tab-2'),
        dcc.Tab(label='Test Case 2 Dynamic', value='tab-3'),
        dcc.Tab(label='Test Case 3 Static', value='tab-4'),
        dcc.Tab(label='Test Case 3 Dynamic', value='tab-4b'),
        dcc.Tab(label='Test Case 4', value='tab-5'),
        dcc.Tab(label='Test Case 5 Static', value='tab-6'),
        dcc.Tab(label='Test Case 5 Dynamic', value='tab-7'),
    ]),
    html.Div(id='tabs-example-content-1')
])


@app.callback(
    Output('tabs-example-content-1', 'children'),
    Input('tabs-example-1', 'value')
)
def render_content(tab):
    if tab == 'tab-1':
        return html.Div(
            [
                html.H3('Test Case 1'),
                dcc.Graph(
                    figure=linecase1
                ),
                html.Data(stats1.all()),
                html.H5('Average of exit right times: ' + str(mean1right)),
                html.H5('Average of exit left times: ' + str(mean1left)),
                html.H5('Standard deviation of exit right times: ' +
                        str(std1right)),
                html.H5('Standard deviation of exit left times: ' + str(std1left)),
                html.H5('Variance of exit right times: ' + str(var1right)),
                html.H5('Variance of exit left times: ' + str(var1left))
            ]
        )
    elif tab == 'tab-2':
        return html.Div([
            html.H3(
                'Test Case 2 before any weight changes, final exit time is 2921.392s'),
            dcc.Graph(
                figure=linecase2
            ),
            html.H5('Average of exit right times: ' + str(mean2right)),
            html.H5('Average of exit left times: ' + str(mean2left)),
            html.H5('Standard deviation of exit right times: ' + str(std2right)),
            html.H5('Standard deviation of exit left times: ' + str(std2left)),
            html.H5('Variance of exit right times: ' + str(var2right)),
            html.H5('Variance of exit left times: ' + str(var2left))
        ])

    elif tab == 'tab-3':
        return html.Div([
            html.H3(
                'Test Case 2 after adjusting weights, final exit time is now 2249.563s'),
            dcc.Graph(
                figure=linecase2Dynamic
            ),
            html.H5('Average of exit right times: ' + str(mean2Dynamicright)),
            html.H5('Average of exit left times: ' + str(mean2Dynamicleft)),
            html.H5('Standard deviation of exit right times: ' +
                    str(std2Dynamicright)),
            html.H5('Standard deviation of exit left times: ' +
                    str(std2Dynamicleft)),
            html.H5('Variance of exit right times: ' + str(var2Dynamicright)),
            html.H5('Variance of exit left times: ' + str(var2Dynamicleft))
        ])

    

    elif tab == 'tab-4':
        return html.Div([
            html.H3('Test Case 3 Static'),
            dcc.Graph(
                figure=linecase3
            ),
            html.H5('Average of exit right times: ' + str(mean3right)),
            html.H5('Average of exit left times: ' + str(mean3left)),
            html.H5('Standard deviation of exit right times: ' + str(std3right)),
            html.H5('Standard deviation of exit left times: ' + str(std3left)),
            html.H5('Variance of exit right times: ' + str(var3right)),
            html.H5('Variance of exit left times: ' + str(var3left))
        ])
    elif tab == 'tab-4b':
        return html.Div([
            html.H3('Test Case 3 Dynamic'),
            dcc.Graph(
                figure=linecase3Dynamic
            ),
            html.H5('Average of exit right times: ' + str(mean3Dynamicright)),
            html.H5('Average of exit left times: ' + str(mean3Dynamicleft)),
            html.H5('Standard deviation of exit right times: ' + str(std3Dynamicright)),
            html.H5('Standard deviation of exit left times: ' + str(std3Dynamicleft)),
            html.H5('Variance of exit right times: ' + str(var3Dynamicright)),
            html.H5('Variance of exit left times: ' + str(var3Dynamicleft))
        ])
    elif tab == 'tab-5':
        return html.Div([
            html.H3('Test Case 4'),
            dcc.Graph(
                figure=linecase4
            ),
            html.H5('Average of exit right times: ' + str(mean4right)),
            html.H5('Average of exit left times: ' + str(mean4left)),
            html.H5('Standard deviation of exit right times: ' + str(std4right)),
            html.H5('Standard deviation of exit left times: ' + str(std4left)),
            html.H5('Variance of exit right times: ' + str(var4right)),
            html.H5('Variance of exit left times: ' + str(var4left))
        ])
    elif tab == 'tab-6':
        return html.Div([
            html.H3(
                'Test Case 5 before any weight changes, final exit time is 2056.188s'),
            dcc.Graph(
                figure=linecase5
            ),
            html.H5('Average of exit right times: ' + str(mean5right)),
            html.H5('Average of exit left times: ' + str(mean5left)),
            html.H5('Standard deviation of exit right times: ' + str(std5right)),
            html.H5('Standard deviation of exit left times: ' + str(std5left)),
            html.H5('Variance of exit right times: ' + str(var5right)),
            html.H5('Variance of exit left times: ' + str(var5left))
        ])
    elif tab == 'tab-7':
        return html.Div([
            html.H3(
                'Test Case 5 after adjusting weights, final exit time is now 1645.851s'),
            dcc.Graph(
                figure=linecase5Dynamic
            ),
            html.H5('Average of exit right times: ' + str(mean5Dynamicright)),
            html.H5('Average of exit left times: ' + str(mean5Dynamicleft)),
            html.H5('Standard deviation of exit right times: ' +
                    str(std5Dynamicright)),
            html.H5('Standard deviation of exit left times: ' +
                    str(std5Dynamicleft)),
            html.H5('Variance of exit right times: ' + str(var5Dynamicright)),
            html.H5('Variance of exit left times: ' + str(var5Dynamicleft))
        ])


if __name__ == '__main__':
    app.run_server(debug=True)
